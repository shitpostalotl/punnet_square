from PunnetGen import Genorate

mode = int(input("Say 1 unless told otherwise or you know what you are doing."))
if mode == 1:

    print("\n\nPlace the following data into Parent 1 \n")
    Genorate("B", "Brown", 'Blonde')
    Genorate("B", "Brown", 'Blue')
    Genorate("R", "Round", 'Oval')
    Genorate("N", "None", 'Dimple')
    Genorate("N", "Narrow", 'Wide')
    Genorate("T", "Thick", 'Thin')
    Genorate("T", "Tall", 'Short')
    Genorate("L", "Long", 'Short')
    Genorate("F", "Big", 'Small')
    Genorate("E", "Big", 'Small')

    print("\n\nPlace the following data into Parent 2 \n")

    Genorate("B", "Brown", 'Blonde')
    Genorate("B", "Brown", 'Blue')
    Genorate("R", "Round", 'Oval')
    Genorate("N", "None", 'Dimple')
    Genorate("N", "Narrow", 'Wide')
    Genorate("T", "Thick", 'Thin')
    Genorate("T", "Tall", 'Short')
    Genorate("L", "Long", 'Short')
    Genorate("F", "Big", 'Small')
    Genorate("E", "Big", 'Small')

    print("\n\n Restart the program and hit 2.")
elif mode == 2:

    P1 = open("Parent 1", "r").readlines()
    P2 = open("Parent 2", "r").readlines()

    g1 = []
    g2 = []

    for P1 in P1: g1.append(P1)
    genes1 = [x.replace('\n', '') for x in g1]

    for P2 in P2: g2.append(P2)
    genes2 = [x.replace('\n', '') for x in g2]

    for i in range(1, 10):
        gen1 = genes1[i]
        gen2 = genes2[i]
        gen1 = gen1[:2]
        gen2 = gen2[:2]

        PSquare1 = str(gen1[:1]) + str(gen2[:1])
        PSquare2 = str(gen1[1:]) + str(gen2[:1])
        PSquare3 = str(gen1[:1]) + str(gen2[1:])
        PSquare4 = str(gen1[1:]) + str(gen2[1:])

        if i == 0: print("Hair: Brown, Blonde (Dominant and Recessive respectively)")
        elif i == 1: print("Eye: Brown, Blue (Dominant and Recessive respectively)")
        elif i == 2: print("Head: Round, Oval (Dominant and Recessive respectively)")
        elif i == 3: print("Dimples: None, Dimple (Dominant and Recessive respectively)")
        elif i == 4: print("Nose: Narrow, Wide (Dominant and Recessive respectively)")
        elif i == 5: print("Lips: Thick, Thin (Dominant and Recessive respectively)")
        elif i == 6: print("Size: Tall, Short (Dominant and Recessive respectively)")
        elif i == 7: print("Arms: Long, Short (Dominant and Recessive respectively)")
        elif i == 8: print("Feet: Big, Small (Dominant and Recessive respectively)")
        elif i == 9: print("Ears: Big, Small (Dominant and Recessive respectively)")

        print("	Punnet's Square\n_______\n|" + PSquare1 + "|" + PSquare2 + "|\n-------\n|" + PSquare3 + "|" + PSquare4 + "|\n-------")

        DOM_Percent = 0
        COMBO_Percent = 0
        RES_Percent = 0

        print("	Genotype")
        if PSquare1.isupper() == True: DOM_Percent += 25
        elif PSquare1.islower() == True: RES_Percent += 25
        else: COMBO_Percent += 25

        if PSquare2.isupper() == True: DOM_Percent += 25
        elif PSquare2.islower() == True: RES_Percent += 25
        else: COMBO_Percent += 25

        if PSquare3.isupper() == True: DOM_Percent += 25
        elif PSquare3.islower() == True: RES_Percent += 25
        else: COMBO_Percent += 25

        if PSquare4.isupper() == True: DOM_Percent += 25
        elif PSquare4.islower() == True: RES_Percent += 25
        else: COMBO_Percent += 25

        print("Homozygous Dominent: " + str(DOM_Percent) + "%\nHeterozygous: " + str(COMBO_Percent) + "%\nHomozygous Recesive: " + str(RES_Percent) + "%\n Phenotype\nDominant Expression: " + str(DOM_Percent + COMBO_Percent) + "%\nRecessive Expression: " + str(RES_Percent) + "%\n")