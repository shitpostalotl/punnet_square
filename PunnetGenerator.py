import random

def Genorate(Symbol, Dominant, Recessive):
    # First Gene
    G1 = random.randint(0, 1)
    if G1 == 0: G1 = "D"
    elif G1 == 1: G1 = "R"

    if G1 == "R": G1 = Symbol.lower()
    if G1 == "D": G1 = Symbol.upper()

    # Second Gene
    G2 = random.randint(0, 1)
    if G2 == 0: G2 = "D"
    if G2 == 1: G2 = "R"

    if G2 == "R": G2 = Symbol.lower()
    elif G2 == "D": G2 = Symbol.upper()

    # Combination
    Combo = (G1 + G2)

    if Combo.islower(): print(Combo + " " + Recessive)
    else: print(Combo + " " + Dominant)